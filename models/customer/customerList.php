<?php

class customerList{

    protected $_dolibarr;
    protected $_db;
    protected  $_customerList;


     public function __construct($dolibarr) {

         $this->setDolibarr($dolibarr);
         $this->setDb($dolibarr->getDb());
        // var_dump($this->selectListCustomer());
         $this->setCustomerList($this->selectListCustomer());
     }

     public function  selectListCustomer()
     {
        return $this->getDb()->querySelect("SELECT  * FROM llx_societe");
     }
    public function  selectListCustomerByNom($nom)
    {
     //   echo "SELECT *   FROM llx_societe,llx_c_country  where llx_c_country.rowid = llx_societe.fk_pays and llx_societe.nom LIKE '%".$nom."%' LIMIT 6";
        return $this->getDb()->querySelect("SELECT *   FROM llx_societe,llx_c_country  where llx_c_country.rowid = llx_societe.fk_pays and llx_societe.nom LIKE '%".$nom."%' LIMIT 6");
    }
    /**
     * @return mixed
     */
    public function getDolibarr()
    {
        return $this->_dolibarr;
    }

    /**
     * @param mixed $dolibarr
     */
    public function setDolibarr($dolibarr)
    {
        $this->_dolibarr = $dolibarr;
    }

    /**
     * @return mixed
     */
    public function getCustomerList()
    {
        return $this->_customerList;
    }

    /**
     * @param mixed $customerList
     */
    public function setCustomerList($list)
    {

        $customerList = array();

        foreach ($list as $customer)
        {
            $objetCustomer= new customer($this->getDolibarr()->getName(),$customer,null);

            $customerList[]=$objetCustomer;
        }
        $this->_customerList = $customerList;
    }
    public function getCustomersRow()
    {
        $list = $this->selectListCustomer();
        $customerList = [];

        foreach ($list as $customer)
        {
            $objetCustomer= new customer($this->getDolibarr()->getName(),$customer,null);
            $customerList[]=$objetCustomer->getRow();
        }
        return $customerList;
    }
    public function getCustomersRowByNom($nom)
    {
        $list = $this->selectListCustomerByNom($nom);
        $customerList = [];

        foreach ($list as $customer)
        {
            $pays=$customer['label'];
            $objetCustomer= new customer($this->getDolibarr()->getName(),$customer, $pays);
         // echo "</br>" .  $objetCustomer->getNom(). "</br>";
            $customerList[]=$objetCustomer->getRow();
        }
        return $customerList;
    }
    /**
     * @return mixed
     */
    public function getDb()
    {
        return $this->_db;
    }

    /**
     * @param mixed $db
     */
    public function setDb($db)
    {
        $this->_db = $db;
    }
}

?>