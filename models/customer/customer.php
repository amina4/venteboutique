<?php

class customer
{
    protected $_dolibarrName;
    protected $_rowid;
    protected $_nom;
    protected $_name_alias;
    protected $_entity;
    protected $_status;
    protected $_code_client;
    protected $_address;
    protected $_zip;
    protected $_town;
    protected $_phone;
    protected $_fax;
    protected $_email;
    protected $_pays;




    public function __construct($dolibarrName, $customer, $pays) {

        $this->setDolibarrName($dolibarrName);
        $this->setRowId($customer['rowid']);
        $this->setNom($customer['nom']);
        $this->setNameAlias($customer['name_alias']);
        $this->setEntity($customer['entity']);
        $this->setStatus($customer['status']);
        $this->setCodeClient($customer['code_client']);
        $this->setAddress($customer['address']);
        $this->setZip($customer['zip']);
        $this->setTown($customer['town']);
        $this->setPhone($customer['phone']);
        $this->setFax($customer['fax']);
        $this->setEmail($customer['email']);
        $this->setPays( $pays);
       // echo    $this->getString();
    }


    public function  getString()
    {
        return " name dolibarr: ".$this->getDolibarrName()."</br";
    }
public  function getRow()
{
    return [
        'dolibarrName'=>$this->getDolibarrName(),
        'rowid'=>$this->getRowId(),
        'nom'=>$this->getNom(),
        'name_alias'=>$this->getNameAlias(),
        'entity'=>$this->getEntity(),
        'status'=>$this->getStatus(),
        'code_client'=>$this->getCodeClient(),
        'address'=>$this->getAddress(),
        'zip'=>$this->getZip(),
        'town'=>$this->getTown(),
        'pays'=>$this->getPays (),
        'phone'=>$this->getPhone(),
        'fax'=>$this->getFax(),
        'email'=>$this->getEmail()
    ];
}
    /**
     * @return mixed
     */
    public function getDolibarrName()
    {
        return $this->_dolibarrName;
    }

    /**
     * @param mixed $dolibarrName
     */
    public function setDolibarrName($dolibarrName)
    {
        $this->_dolibarrName = $dolibarrName;
    }

    /**
     * @return mixed
     */
    public function getRowId()
    {

        return $this->_rowid;
    }

    /**
     * @param mixed $rowid
     */
    public function setRowId($rowid)
    {
        $this->_rowid = $rowid;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->_nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->_nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getNameAlias()
    {
        return $this->_name_alias;
    }

    /**
     * @param mixed $name_alias
     */
    public function setNameAlias($name_alias)
    {
        $this->_name_alias = $name_alias;
    }

    /**
     * @return mixed
     */
    public function getEntity()
    {
        return $this->_entity;
    }

    /**
     * @param mixed $entity
     */
    public function setEntity($entity)
    {
        $this->_entity = $entity;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->_status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->_status = $status;
    }

    /**
     * @return mixed
     */
    public function getCodeClient()
    {
        return $this->_code_client;
    }

    /**
     * @param mixed $code_client
     */
    public function setCodeClient($code_client)
    {
        $this->_code_client = $code_client;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->_address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->_address = $address;
    }

    /**
     * @return mixed
     */
    public function getZip()
    {
        return $this->_zip;
    }

    /**
     * @param mixed $zip
     */
    public function setZip($zip)
    {
        $this->_zip = $zip;
    }

    /**
     * @return mixed
     */
    public function getTown()
    {
        return $this->_town;
    }

    /**
     * @param mixed $town
     */
    public function setTown($town)
    {
        $this->_town = $town;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->_phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->_phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getFax()
    {
        return $this->_fax;
    }

    /**
     * @param mixed $fax
     */
    public function setFax($fax)
    {
        $this->_fax = $fax;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->_email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->_email = $email;
    }
    /**
     * @return mixed
     */
    public function getPays()
    {
        return $this->_pays;
    }

    /**
     * @param mixed $pays
     */
    public function setPays($pays)
    {
        $this->_pays = $pays;
    }
}
?>