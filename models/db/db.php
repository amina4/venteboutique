<?php

class db{

    protected $_connection;
    protected $_dbuser;
    protected $_dbhost;
    protected $_dbpass;
    protected $_dbname;
    public function __construct($conf) {
        $this->setDbuser($conf['dbuser']);
        $this->setDbhost($conf['dbhost']);
        $this->setDbpass($conf['dbpass']);
        $this->setDbname($conf['dbname']);
        $this->setConnection();
        // echo "ok";
    }

    public function setConnection()
    {
        // array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8') c'est la solution pour les carat�res sp�ciaux
        try{
            $this->_connection= new PDO('mysql:host='.$this->getDbhost().';dbname='.$this->getDbname(), $this->getDbuser(), $this->getDbpass(), array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));

        }catch(Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }
    public function querySelect($query)
    {
        $res = $this->getConnection()->prepare($query);
        $res->execute();
        return $res->fetchAll();
    }
    public function queryInsert($query)
    {
        $res = $this->getConnection()->query($query);
        if($res)
        {
            // echo "insert </br>";
        }
    }
    public function queryUpdate($query)
    {
        $res = $this->getConnection()->prepare($query);
        $res->execute();
    }
    public function getConnection()
    {
        return $this->_connection;
    }
    public function setDbuser($dbuser)
    {
        $this->_dbuser=$dbuser;
    }
    public function setDbhost($dbhost)
    {
        $this->_dbhost = $dbhost;
    }
    public function setDbpass($dbpass)
    {
        $this->_dbpass=$dbpass;
    }
    public function setDbname($dbname)
    {
        $this->_dbname=$dbname;
    }
    public function getDbuser()
    {
        return $this->_dbuser;
    }
    public function getDbhost()
    {
        return $this->_dbhost;
    }
    public function getDbpass()
    {
        return $this->_dbpass;
    }

    public function getDbname()
    {
        return $this->_dbname;
    }
}

?>