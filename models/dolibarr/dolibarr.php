<?php
class dolibarr{

    protected $_db;
    protected $_name;
    protected $_paymentCb;
    protected $_paymentCash;
    protected  $_customerList;



    public function __construct($db,$name,$paymentCb,$paymentCash) {
        $this->setDb($db);
        $this->setName($name);
        $this->setPaymentCb($paymentCb);
        $this->setPaymentCash($paymentCash);
        $this->setCustomerList(new customerList($this));


    }

    public function  getString()
    {
        return "</br> name : ".$this->getName()." payment_cash ".$this->getPaymentCash()." payment_cb ".$this->getPaymentCb()." </br>";
    }
    public function getCustomersRowBynom($nom)
    {
       // echo "</br> i am here </br>";
       // var_dump ($this->getCustomerList()->getCustomersRowByNom($nom));
        return $this->getCustomerList()->getCustomersRowByNom($nom);
    }
    public function getCustomersRow()
    {
        return $this->getCustomerList()->getCustomersRow();
    }
    /**
     * @return mixed
     */
    public function getCustomerList()
    {
        return $this->_customerList;
    }

    /**
     * @param mixed $customerList
     */
    public function setCustomerList($customerList)
    {
        $this->_customerList = $customerList;
    }
    /**
     * @return mixed
     */
    public function getDb()
    {
        return $this->_db;
    }

    /**
     * @param mixed $db
     */
    public function setDb($db)
    {
        $this->_db = $db;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * @return mixed
     */
    public function getPaymentCb()
    {
        return $this->_paymentCb;
    }

    /**
     * @param mixed $paymentCb
     */
    public function setPaymentCb($paymentCb)
    {
        $this->_paymentCb = $paymentCb;
    }

    /**
     * @return mixed
     */
    public function getPaymentCash()
    {
        return $this->_paymentCash;
    }

    /**
     * @param mixed $paymentCash
     */
    public function setPaymentCash($paymentCash)
    {
        $this->_paymentCash = $paymentCash;
    }

}
?>