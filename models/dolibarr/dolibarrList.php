<?php

class dolibarrList{

    protected  $_dolibarrList;
    protected $_CustomerList;

    public function  __construct($conf) {

        $this->setDolibarrList($this->setUpDolibarr($conf));
    }
    /**
     * @return mixed
     */
    public function getDolibarrList()
    {
        return $this->_dolibarrList;
    }
    public function getDolibarrListString()
    {
       /* foreach ($this->_dolibarrList as $doli)
          echo  $doli->getString();*/
    }
    /**
     * @param mixed $dolibarrList
     */
    public function setDolibarrList($dolibarrList)
    {
        $this->_dolibarrList = $dolibarrList;
    }
  public function  setUpDolibarr($conf)
  {
      $dolibarrList=array();
      foreach ($conf as $c)
      {
         $dolibarr= new dolibarr(new db($c['db']),$c['name'],$c['paymentCb'],$c['paymentCash']);
          $dolibarrList[]=$dolibarr;
      }
     // var_dump($dolibarrList);
      return $dolibarrList;
  }

    /**
     * @return mixed with filter
     */
    public function getCustomerListByNom($nom)
    {
      //  echo $nom;
        $list = [];
        foreach ($this->_dolibarrList as $dolibarr)
        {
            $list[]= $dolibarr->getCustomersRowByNom($nom);
        }
        $this->_CustomerList=$list;
        return $this->_CustomerList;
    }

    /**
     * @return mixed
     */
    public function getCustomerList()
    {
        $list = [];
        foreach ($this->_dolibarrList as $dolibarr)
        {
            $list[]= $dolibarr->getCustomersRow();
        }
        $this->_CustomerList=$list;
        return $this->_CustomerList;
    }

    /**
     * @param mixed $CustomerList
     */
    public function setCustomerList($CustomerList)
    {
        $this->_CustomerList = $CustomerList;
    }
}
?>