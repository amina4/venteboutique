<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">

    <title>Vente boutique</title>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Sonsie+One" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
      <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
      <script>
          $(document).ready(function(){


          });
      </script>
  </head>

  <body>
    <!-- Voici notre en‑tête principale utilisée dans toutes les pages
         de notre site web -->
    <header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">Pixels-art</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item active">
        <a class="nav-link" href="#">Accuiel<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Mes commandes</a>
      </li>
    </ul>
    <div class="form-inline my-2 my-lg-0">

      <button class="btn  my-2 my-sm-0" type="submit"><i class="fa fa-user"></i> amina chguifi</button>
    </div>
  </div>
</nav>
    </header>
   
    <main>
       <div class="contain">
           <div class="row top">   <hr> </div>
           <!-- start customer part -->
           <div class="form-customer row">
               <div class=" col-md-1"></div>
               <div class=" col-md-2"><h3>Client</h3> </div>
           </div>
           <div class=" form-customer row">
               <di class=" col-md-1"></di>

           <div class="form-customer col-md-9">
           <form method="" action="">
               <div class="row">
                   <div class="col-md-3">
                       <label>Nom prenom</label>
                   </div>
               </div>
               <div class="row">
                   <div class="col-md-6 listCustomer">

                       <input list="customers" type="text" class="form-control"  placeholder="Nom prenom" id="nom">
                     <!--  <div class='customers'></div>-->
                     <datalist id='customers'>
                           <option id='"+i+"' class='res' value='ok' style="background-color:red;"></option>
                       </datalist>

                   </div>
                   <div class="col-sm-1">
                       <button class="btn  btn-info btn-sm" id="check" type="button"><i class="fa fa-check"></i></button>
                   <!--   <button class="btn  btn-success btn-sm" id="" type="button"><i class="fa fa-chevron-up"></i></button>-->
                   </div>
                   <div class="col-sm-3"></div>
                   <div class="col-sm-2">
                       <button class="btn  btn-success btn-md col-md-12" id="up" type="button">Confirmer</button>
                   </div>
               </div>

               <div class="form-row customer">
                   <div class="form-group col-md-3">
                       <label>Code client</label>
                       <input type="text" class="form-control" id="inputCode" placeholder="Code client">
                   </div>
                   <div class="form-group col-md-6">
                       <label>Email</label>
                       <input type="email" class="form-control" id="inputEmail" placeholder="Email">
                   </div>
                   <div class="form-group col-md-3">
                       <label>Phone</label>
                       <input type="phone" class="form-control" id="inputPhone" placeholder="Phone">
                   </div>
               </div>
               <div class="form-row customer">
                   <div class="form-group col-md-6">
                       <label>Address</label>
                       <input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St">
                   </div>
                   <div class="form-group col-md-4">
                       <label>Ville</label>
                       <input type="text" class="form-control" id="inputCity" placeholder="ville">
                   </div>
               </div>
                   <div class="form-row customer">

                       <div class="form-group col-md-4">
                           <label>Pays</label>
                           <input type="text" class="form-control" id="inputCountry" placeholder="pays">
                       </div>
                       <div class="form-group col-md-4">
                           <label>Code postal</label>
                           <input type="text" class="form-control" id="inputZip" placeholder="75018">
                       </div>
                   </div>

           </form>
           </div>

           </div>
           <div class="row">
               <div class="col-md-3"></div>
               <div class="col-md-6">

               </div>
               <div class="col-md-3"></div>
           </div>
           <!-- end customer part -->
           <hr>
           <!-- start product prt -->
           <div class="row products">

               <div class="product-tab row">
                   <div class="col-md-1"></div>
                   <div class="col-md-10">
                       <table class="table table-hover">
                           <thead>
                           <tr>
                               <th scope="col">Produits</th>
                               <th scope="col">TVA</th>
                               <th scope="col">P.H.T</th>
                               <th scope="col">P.U T.T.C</th>
                               <th scope="col">Q</th>
                               <th scope="col">Reduc</th>
                               <th scope="col">T.H.T</th>
                               <th scope="col">T.T.C</th>
                               <th scope="col"></th>
                               <th scope="col"></th>
                               <th scope="col"></th>
                           </tr>
                           </thead>
                           <tbody>
                           <tr>
                               <th scope="row"><input type="text" class="form-control col-sm-12" >
                               </th>
                               <td>
                                   <select id="inputState" class="form-control col-sm-12">
                                       <option selected>10%</option>
                                       <option>20%</option>
                                   </select>
                               </td>
                               <td><input type="text" class="form-control col-sm-12" ></td>
                               <td><input type="text" class="form-control col-sm-12" ></td>
                               <td><input type="number" class="form-control col-sm-10" ></td>
                               <td><div class="col-auto">
                                       <div class="input-group mb-12">
                                           <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="0">
                                           <div class="input-group-prepend">
                                               <div class="input-group-text">%</div>
                                           </div>
                                       </div>
                                   </div></td>
                               <td><input type="text" class="form-control col-sm-12"  value="120.20" disabled></td>
                               <td><input type="text" class="form-control col-sm-12"  value="120.20" disabled></td>
                               <td><button class="btn  btn-primary btn-sm" type="submit"><i class="fa fa-edit"></i> </button></td>
                               <td><button class="btn  btn-danger btn-sm" type="submit"> <i class="fa fa-trash"></i></button></td>
                               <td><button class="btn  btn-success btn-sm" type="submit"><i class="fa fa-plus"></i></button></td>

                           </tr>
                           </tbody>
                       </table>
                   </div>
               </div>
           </div>
           <!-- end  produit part -->
           <!-- start  payment part -->
           <hr>
           <div class="row">
               <div class="col-md-1"></div>
               <div class="col-md-2"><h3>Mode de paiement</h3> </div>
               <div class="col-md-4">
                       <input class="form-check-input" type="checkbox" name="payment" id="autoSizingCheck"><i class="fa fa-money"></i>

                   <div class="col-auto">
                       <div class="input-group">
                           <input type="text" class="form-control col-sm-4" >
                           <div class="input-group-prepend">
                               <div class="input-group-text">€</div>
                           </div>
                       </div>
                   </div>

               </div>

               <div class="col-md-4">
                   <input class="form-check-input" type="checkbox"  name="payment" id="autoSizingCheck"><i class="fa fa-credit-card"></i><br>
                   <div class="col-auto">
                       <div class="input-group">
                           <input type="text" class="form-control col-sm-4" >
                           <div class="input-group-prepend">
                               <div class="input-group-text">€</div>
                           </div>
                       </div>
                   </div>

               </div>

           </div>
           <!-- end payment part-->
           <div class="row" style="height:3em"></div>
           <div class="row">

               <div class="col-lg-4"></div>
               <div class="col-lg-6">
                   <button class="btn  btn-primary col-lg-4" type="submit"><i class="fa fa-print"></i> imprimer</button>
                   <button class="btn  btn-danger col-lg-4" type="submit"><i class="fa fa-envelope"></i> E-mail</button>
               </div>
           </div>

       </div>
        <!-- end  contain part-->
    </main>

    <!-- Et voici notre pied de page utilisé sur toutes les pages du site -->
    <footer>

    </footer>

    <script type="application/javascript" src="js/getCustomer.js">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>